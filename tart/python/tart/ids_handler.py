from bb import bps
from bb.ids import *
from ctypes import get_errno, POINTER, byref, c_char, c_char_p, cast, create_string_buffer, c_int, c_uint, sizeof, c_void_p, py_object, string_at
import abc

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

class IDSException(Exception):
    '''represents failure in IDS call'''
    def __init__(self, message, request_id = None, result = None, info = None, data = None):
        super().__init__(message)
        self.request_id = request_id
        self.result = result
        self.info = info
        self.data = data

    def __str__(self):
        s = super().__str__()
        return "%s (request_id = %s, result = %s, info = %s, data = %s)" % (s, self.request_id, self.result, self.info, self.data)

class IDSHandler(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self._ids_provider = POINTER(ids_provider_t)()
        self._ids_fd = c_int(-1)
        self._ids_req_id = c_uint(0)
        self._ids_io_handler_callback = bps.bps_add_fd_cb_func(
                IDSHandler._ids_io_handler)
        self._ids_call_failed_callback = failure_cb_func(self._ids_call_failed)

        # First initialize the IDS framework
        rc = ids_initialize()
        if rc != IDS_SUCCESS:
            raise IDSException("Failure to initialize. errno: %d"
                    % get_errno())
        self._register_provider()
        self._monitor_file_descriptor()

    def _register_provider(self):
        # Register the app with the provider
        rc = ids_register_provider(
                self._provider_id,
                byref(self._ids_provider), 
                byref(self._ids_fd))
        if rc != IDS_SUCCESS:
            raise IDSException("Failure to register. errno: %d"
                    % get_errno())

    def _monitor_file_descriptor(self):
        # Start monitoring the file descriptor
        rc = bps.bps_add_fd(
                self._ids_fd, 
                bps.BPS_IO_INPUT, 
                self._ids_io_handler_callback, 
                None)
        if rc != bps.BPS_SUCCESS:
            raise IDSException("Failed to monitor fd. errno: %d" % get_errno())

    @staticmethod
    def _ids_io_handler(fd, bps_io_events, user_data):
        rc = ids_process_msg(fd)
        if rc == IDS_SUCCESS:
            return bps.BPS_SUCCESS
        else:
            # This will stop monitoring the file descriptor
            return bps.BPS_FAILURE

    def _ids_call_failed(self, request_id, result, info, data):
        ''' Override this to handle failures appropriately. '''
        print(request_id, result, info, data)

    @abc.abstractproperty
    def _provider_id(self):
        ''' Return the provider ID. '''

class IDS_BBID_Handler(IDSHandler):
    _props = (c_char_p * 5)()
    _props[0] = IDS_BBID_PROP_USERNAME
    _props[1] = IDS_BBID_PROP_SCREENNAME
    _props[2] = IDS_BBID_PROP_FIRSTNAME
    _props[3] = IDS_BBID_PROP_LASTNAME
    _props[4] = IDS_BBID_PROP_UID

    def __init__(self):
        super().__init__()
        self._bbid_got_username_callback = get_properties_cb_func(
                self._got_username)
        self._bbid_got_screenname_callback = get_properties_cb_func(
                self._got_screenname)
        self._bbid_got_firstname_callback = get_properties_cb_func(
                self._got_firstname)
        self._bbid_got_lastname_callback = get_properties_cb_func(
                self._got_lastname)
        self._bbid_got_uid_callback = get_properties_cb_func(
                self._got_uid)

    @property
    def _provider_id(self):
        return BLACKBERRY_ID_PROVIDER

    def getUserName(self):
        rc = ids_get_properties(
                self._ids_provider, 
                BBID_PROPERTY_CORE, 
                1, 
                cast(byref(self._props, sizeof(c_char_p) * 0), 
                    POINTER(c_char_p)), 
                self._bbid_got_username_callback, 
                self._ids_call_failed_callback, 
                None,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get user name. errno: %d" 
                    % get_errno())

    def _got_username(self, request_id, property_count, properties, data):
        self.onGotUserName(properties[0].value.decode('utf-8'))

    @abc.abstractmethod
    def onGotUserName(self, username):
        ''' Called when we have the username. '''

    def getScreenName(self):
        rc = ids_get_properties(
                self._ids_provider, 
                BBID_PROPERTY_CORE, 
                1, 
                cast(byref(self._props, sizeof(c_char_p) * 1), 
                    POINTER(c_char_p)), 
                self._bbid_got_screenname_callback, 
                self._ids_call_failed_callback, 
                None,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get screen name. errno: %d" 
                    % get_errno())

    def _got_screenname(self, request_id, property_count, properties, data):
        self.onGotScreenName(properties[0].value.decode('utf-8'))

    @abc.abstractmethod
    def onGotScreenName(self, screenname):
        ''' Called when we have the screen name. '''

    def getFirstName(self):
        rc = ids_get_properties(
                self._ids_provider, 
                BBID_PROPERTY_CORE, 
                1, 
                cast(byref(self._props, sizeof(c_char_p) * 2), 
                    POINTER(c_char_p)), 
                self._bbid_got_firstname_callback, 
                self._ids_call_failed_callback, 
                None,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get first name. errno: %d" 
                    % get_errno())

    def _got_firstname(self, request_id, property_count, properties, data):
        self.onGotFirstName(properties[0].value.decode('utf-8'))

    @abc.abstractmethod
    def onGotFirstName(self, screenname):
        ''' Called when we have the first name. '''

    def getLastName(self):
        rc = ids_get_properties(
                self._ids_provider, 
                BBID_PROPERTY_CORE, 
                1, 
                cast(byref(self._props, sizeof(c_char_p) * 3), 
                    POINTER(c_char_p)), 
                self._bbid_got_lastname_callback, 
                self._ids_call_failed_callback, 
                None,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get last name. errno: %d" 
                    % get_errno())

    def _got_lastname(self, request_id, property_count, properties, data):
        self.onGotLastName(properties[0].value.decode('utf-8'))

    @abc.abstractmethod
    def onGotLastName(self, lastname):
        ''' Called when we have the last name. '''

    def getUID(self):
        rc = ids_get_properties(
                self._ids_provider, 
                BBID_PROPERTY_CORE, 
                1, 
                cast(byref(self._props, sizeof(c_char_p) * 4), 
                    POINTER(c_char_p)), 
                self._bbid_got_uid_callback, 
                self._ids_call_failed_callback, 
                None,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get UID. errno: %d" 
                    % get_errno())

    def _got_uid(self, request_id, property_count, properties, data):
        self.onGotUID(properties[0].value.decode('utf-8'))

    @abc.abstractmethod
    def onGotUID(self, screenname):
        ''' Called when we have the UID. '''

class IDS_BBProfile_Handler(IDSHandler):
    """Implementation for BBID off-device storage"""

    StorageType = enum("DEFAULT", "CACHED", "ALL", "ENCRYPTED")

    class PendingData(object):
        def __init__(self):
            self.request_id = c_uint(0)
            self.name = None
            self.value = None
            self.length = None
            self.data = None
            self._user_data = None

    def __init__(self):
        super().__init__()
        self._bbprofile_got_list_callback = list_cb_func(self._got_list_data)
        self._bbprofile_created_data_callback = success_cb_func(self._created_data)
        self._bbprofile_deleted_data_callback = success_cb_func(self._deleted_data)
        self._bbprofile_got_data_callback = get_data_cb_func(self._got_data)
        self._bbprofile_set_data_callback = success_cb_func(self._set_data)
        self._bbprofile_register_notifier_callback = notify_cb_func(self._notify_callback)
        self._create_data_list = None
        self._delete_data_list = None
        self._get_data_list = None
        self._set_data_list = None

    @property
    def _provider_id(self):
        return BLACKBERRY_PROFILE_PROVIDER

    def getListOfData(self, callback = None):
        rc = ids_list_data(
                self._ids_provider,
                self._profile_type,
                IDS_PROFILE_LIST_DATA_DEFAULT,
                self._bbprofile_got_list_callback,
                self._ids_call_failed_callback,
                callback,
                byref(self._ids_req_id))
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get list of data. errno: %d" 
                    % get_errno())

    def _got_list_data(self, request_id, list_count, list_data, callback):
        data = list()
        for i in range(list_count):
            data.append(list_data[i].decode('utf-8'))
        obj = dict()
        obj['data'] = data
        obj['request_id'] = request_id
        if callback is not None:
            callback(True, obj)
        else:
            self.onGotListOfData(obj)

    def createData(self, name, value, 
            storage_type = StorageType.DEFAULT, callback = None):
        create_data_type = IDS_PROFILE_CREATE_DATA_DEFAULT
        if storage_type == IDS_BBProfile_Handler.StorageType.ENCRYPTED:
            create_data_type = IDS_PROFILE_CREATE_DATA_ENCRYPT_D2D
        elif storage_type == IDS_BBProfile_Handler.StorageType.CACHED:
            create_data_type = IDS_PROFILE_CREATE_DATA_CACHE

        if self._create_data_list == None:
            self._create_data_list = list()

        data = IDS_BBProfile_Handler.PendingData()
        data.name = cast(create_string_buffer(name.encode('utf-8')), c_char_p)
        data.value = cast(value, c_void_p)
        data.length = len(value)
        data.data = ids_data_t(data.name, data.value, data.length)
        rc = ids_create_data(
                self._ids_provider,
                self._profile_type,
                create_data_type,
                byref(data.data),
                self._bbprofile_created_data_callback,
                self._ids_call_failed_callback,
                callback,
                byref(data.request_id))
        self._create_data_list.append(data)
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to create data. errno: %d" 
                    % get_errno())

    def _created_data(self, request_id, callback):
        for data in self._create_data_list:
            if data.request_id.value == request_id:
                self._create_data_list.remove(data)
                obj = dict()
                obj['data'] = data
                obj['request_id'] = request_id
                if callback is not None:
                    callback(True, obj)
                else:
                    self.onCreatedData(obj)

    def deleteData(self, name, storage_type = StorageType.DEFAULT, 
            callback = None):
        delete_data_type = IDS_PROFILE_DELETE_DATA_DEFAULT
        if storage_type == IDS_BBProfile_Handler.StorageType.CACHED:
            delete_data_type = IDS_PROFILE_DELETE_DATA_CACHE_ONLY
        elif storage_type == IDS_BBProfile_Handler.StorageType.ALL:
            delete_data_type = IDS_PROFILE_DELETE_DATA_ALL

        if self._delete_data_list == None:
            self._delete_data_list = list()

        data = IDS_BBProfile_Handler.PendingData()
        data.name = cast(create_string_buffer(name.encode('utf-8')), c_char_p)
        rc = ids_delete_data(
                self._ids_provider,
                self._profile_type,
                delete_data_type,
                data.name,
                self._bbprofile_deleted_data_callback,
                self._ids_call_failed_callback,
                callback,
                byref(data.request_id))
        self._delete_data_list.append(data)
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to delete data. errno: %d" 
                    % get_errno())

    def _deleted_data(self, request_id, callback):
        for data in self._delete_data_list:
            if data.request_id.value == request_id:
                self._delete_data_list.remove(data)
                obj = dict()
                obj['data'] = data
                obj['request_id'] = request_id
                if callback is not None:
                    callback(True, obj)
                else:
                    self.onDeletedData(obj)

    def getData(self, name, storage_type = StorageType.DEFAULT, 
            callback = None):
        get_data_type = IDS_PROFILE_GET_DATA_DEFAULT
        if storage_type == IDS_BBProfile_Handler.StorageType.CACHED:
            get_data_type = IDS_PROFILE_GET_DATA_CACHE

        if self._get_data_list == None:
            self._get_data_list = list()

        data = IDS_BBProfile_Handler.PendingData()
        data.name = cast(create_string_buffer(name.encode('utf-8')), c_char_p)
        rc = ids_get_data(
                self._ids_provider,
                self._profile_type,
                get_data_type,
                data.name,
                self._bbprofile_got_data_callback,
                self._ids_call_failed_callback,
                callback,
                byref(data.request_id))
        self._get_data_list.append(data)
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to get data. errno: %d" 
                    % get_errno())

    def _got_data(self, request_id, data, callback):
        for item in self._get_data_list:
            if item.request_id.value == request_id:
                value = string_at(data.contents.value, data.contents.length)
                kv = dict()
                kv[data.contents.name.decode('utf-8')] = value
                obj = dict()
                obj['data'] = kv
                obj['request_id'] = request_id
                if callback is not None:
                    callback(True, obj)
                else:
                    self.onGotData(obj)

    def setData(self, name, value, storage_type = StorageType.DEFAULT, callback = None):
        set_data_type = IDS_PROFILE_SET_DATA_DEFAULT
        if storage_type == IDS_BBProfile_Handler.StorageType.CACHED:
            set_data_type = IDS_PROFILE_SET_DATA_CACHE

        if self._set_data_list == None:
            self._set_data_list = list()

        data = IDS_BBProfile_Handler.PendingData()
        data.name = cast(create_string_buffer(name.encode('utf-8')), c_char_p)
        data.value = cast(value, c_void_p)
        data.length = len(value)
        data.data = ids_data_t(data.name, data.value, data.length)
        rc = ids_set_data(
                self._ids_provider,
                self._profile_type,
                set_data_type,
                byref(data.data),
                self._bbprofile_set_data_callback,
                self._ids_call_failed_callback,
                callback,
                byref(data.request_id))
        self._set_data_list.append(data)
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to set data. errno: %d" 
                    % get_errno())

    def _set_data(self, request_id, callback):
        for data in self._set_data_list:
            if data.request_id.value == request_id:
                self._set_data_list.remove(data)
                obj = dict()
                obj['data'] = data
                obj['request_id'] = request_id
                if callback is not None:
                    callback(True, obj)
                else:
                    self.onSetData(obj)

    def registerNotifier(self, name):
        register_notifier_type = IDS_PROFILE_NOTIFIER_START

        rc = ids_register_notifier(
                self._ids_provider,
                self._profile_type,
                register_notifier_type,
                cast(create_string_buffer(name.encode('utf-8')), c_char_p),
                self._bbprofile_register_notifier_callback,
                None)
        if rc != IDS_SUCCESS:
            raise IDSException("Failed to register notifier. errno: %d" 
                    % get_errno())

    def _notify_callback(self, profile_type, name, notification, user_data):
        print('notify_callback')
        self.onNotify(profile_type, name.decode('utf-8'), notification)

    @abc.abstractmethod
    def onGotListOfData(self, data):
        ''' Called when we've received the list of data. '''

    @abc.abstractmethod
    def onCreatedData(self, data):
        ''' Called when we've created the data. '''

    @abc.abstractmethod
    def onDeletedData(self, data):
        ''' Called when we've deleted the data. '''

    @abc.abstractmethod
    def onGotData(self, data):
        ''' Called when we've received the data. '''

    @abc.abstractmethod
    def onSetData(self, data):
        ''' Called when we've finished setting the data. '''

    @abc.abstractmethod
    def onNotify(self, profile_type, name, notification):
        ''' Called when we're notified about a key we registered for. '''

    @abc.abstractproperty
    def _profile_type(self):
        ''' Return either app or vendor for the type of data wanted. '''
