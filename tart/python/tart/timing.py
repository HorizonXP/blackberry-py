'''BPS-based time service via sigevents.'''

from ctypes import c_int, byref, get_errno

from bb import bps
from bb.siginfo import sigevent
from bb import time as bt

# from stdlib.h
EXIT_SUCCESS = 0
EXIT_FAILURE = 1


class Timer:
    def __init__(self, handler):
        self.running = False
        self.id = c_int(0)

        self._sigev = sigevent()
        rc = bps.bps_add_sigevent_handler(byref(self._sigev), handler, id(self))
        if rc:
            print('bps_add_sigevent_handler error', get_errno())
        else:
            print('added sigevent handler', rc)
            # dump_sigevent(sigev)

        rc = bt.timer_create(bt.CLOCK_SOFTTIME, byref(self._sigev), byref(self.id))
        if rc:
            print(self, 'timer_create:', get_errno())
            return

        print(self, 'created')


    def __repr__(self):
        r = ['<Timer 0x%08x' % id(self)]
        r.append(' #%d' % self.id.value)
        r.append('>')
        return ''.join(r)


    def __del__(self):
        if self._sigev:
            rc = bps.bps_remove_sigevent_handler(byref(self._sigev))
            if rc:
                print('bps_remove_sigevent_handler:', get_errno())


    def set_delay(self, delay):
        tspec = bt.itimerspec()
        tspec.it_value.tv_sec = int(delay)
        tspec.it_value.tv_nsec = int((delay % 1) * 1000000000)
        rc = bt.timer_settime(self.id, 0, byref(tspec), None)
        if rc:
            print(self, 'timer_settime:', get_errno())


    def set_callback(self, callback, args):
        self.callback = callback
        self.cb_args = args


    def fire(self):
        if self.callback:
            self.callback(*self.cb_args)

        # could do a repeating timer here
        # delay = 1.7
        # tspec = itimerspec()
        # tspec.it_value.tv_sec = int(delay)
        # tspec.it_value.tv_nsec = int((delay % 1) * 1000000000)
        # self.timer_settime(timerid, 0, byref(tspec), None)



class TimerHandler:
    def __init__(self, debug=False):
        self.active = {}
        self.cache = set()

        # called when sigevent fires when timer expires
        def _sigevent_callback(data):
            print('_sigevent_callback', data)
            self._handle_timer(data)
            return EXIT_SUCCESS

        self._sigevent_cb = bps.sigevent_handler_func(_sigevent_callback)


    def start_timer(self, delay, callback, *args):
        print('start timer', delay, callback, args)

        try:
            timer = self.cache.pop()
        except KeyError:
            timer = Timer(self._sigevent_cb)

        timer.set_delay(delay)
        timer.set_callback(callback, args)

        timer.running = True
        self.active[id(timer)] = timer

        return timer


    def _handle_timer(self, tid):
        try:
            timer = self.active.pop(tid)
        except KeyError:
            print('timer not found:', tid)
        else:
            print('timer expired', tid, timer)

            timer.running = False
            timer.fire()

            # TODO: add some statistical tracking and keep the cache trim
            # by not returning things here if, say, we've already got
            # 25% of our highest in-use count to date, combined with a
            # minimum and maybe some timing so we don't dump the cache
            # too quickly. Or whatever.
            self.cache.add(timer)


# EOF
